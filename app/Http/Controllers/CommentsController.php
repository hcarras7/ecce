<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Comments;
use App\Http\Requests\CommentsRequest;
use App\Http\Requests\DataCommentRequest;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        $comments = [];
        if(Comments::all()->first()) {
            $comments = Comments::all()->first()->get();
        }

        return view('comments.list_comments', compact('comments'));    
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('comments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Comments $comments, CommentsRequest $request)
    {
        //
        //dd($request->all());
        $comments->create($request->all());
        return redirect('comments')->with('message', 'It has created a new comment');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //

        $comment = Comments::find($id);
        $comment->delete();
        return redirect('comments')->with('message', 'Comment destroyed');
    }

    /**
     *
     */
    public function postUpdateComment(DataCommentRequest $request)
    {
        $data = $request->all();
        $comment = Comments::find($data['pk']);
        $comment->$data['name'] = $data['value'];
        $comment->save();
        return response()->json('ok', 200);
    }
}
