$(document).ready( function() {
	var route = $('#tbody-url').attr('route');
	$.ajaxSetup({headers: {'X-CSRF-TOKEN': $( 'meta[name="csrf_token"]' ).attr( 'content' ) }})

	function initXeditable()
	{
		$('.x-name').editable({
			type: 'text',
			name: 'name',
			url: route,
			title: 'Enter name',
			ajaxOptions: {
			    dataType: 'json'
			}
		});

		$('.x-mail').editable({
			type: 'text',
			name: 'mail',
			url: route,
			title: 'Enter mail',
			ajaxOptions: {
			    dataType: 'json'
			}
		});

		$('.x-age').editable({
			type: 'text',
			name: 'age',
			url: route,
			title: 'Enter age',
			ajaxOptions: {
			    dataType: 'json'
			}
		});

		$('.x-title').editable({
			type: 'text',
			name: 'title',
			url: route,
			title: 'Enter title',
			ajaxOptions: {
			    dataType: 'json'
			}
		});
		$('.x-comment').editable({
			type: 'text',
			name: 'comment',
			url: route,
			title: 'Enter Coemment',
			ajaxOptions: {
			    dataType: 'json'
			}
		});
	}

	initXeditable();

	$(document).on('click', '.modal-delete', function(){
	  message = $(this).attr('message');
	  route = $(this).attr('route');
	  $('#form-modal-delete').attr('action', route);
	  $('#message').empty().append(message);
	});
});