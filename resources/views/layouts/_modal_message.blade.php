<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
  
      <div class="modal-header" style="background-color:#d9534f;">
        <h4 class="modal-title" style="color:white;text-align:center;">Confirmation</h4>
      </div>
  
      <div class="modal-body">
        <h4 class="text-warning" id="message">(_message)</h4>
      </div>
      
      <div class="modal-footer">
        {!! Form::open(['method' => 'DELETE', 'class' => 'form-horizontal', 'url' => '', 'id' => 'form-modal-delete']) !!}
            {!! Form::submit('Accept', ['class' => 'btn btn-danger']) !!}
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>