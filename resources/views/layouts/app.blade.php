<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf_token" content="{!! csrf_token() !!}">
  <meta content="authenticity_token" name="csrf-param" />
	<title>Ecce</title>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
  <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" href="{{ url('css/comments.css') }}">
</head>
<body>
  @include('layouts._nav')
  @include('layouts._modal_message')
  <div class="container-fluid">
    @yield('content')
  </div>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
  <script src="{{ url('js/script.js') }}"></script>
  <script src="//cdn.ckeditor.com/4.4.7/full/ckeditor.js"></script>

  <script type="text/javascript">

    CKEDITOR.replace( 'comment',
        {
            //TODO: mover esta configuración a un archivo -> customConfig: 'js/ckeditor.js'
            language: 'en',
            toolbarGroups: [
                { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
                { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
                { name: 'insert' },
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
                { name: 'styles' },
                { name: 'colors' }
            ]

        }
    );

</script>
</body>
</html>
