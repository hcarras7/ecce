<!-- Aquí van los mensajes en caso de que existan -->
@if (Session::has('message'))
   <div class="alert alert-success {{ Session::has('message_important') ? 'alert-important' : '' }} ">
    @if(Session::has('message_important'))
      <button class='close' type='button' data-dismiss='alert' aria-hidden='true'>&times;</button>
    @endif
    {{ session('message') }}
   </div>
@endif
