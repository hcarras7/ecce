@extends('layouts.app')

@section('content')
	<div id="wrapper-comments">
		<div class="title-comments"><span>Menu de Comentarios</span></div>
		<div id="wrapper-options">
			<div class="menu-option"><span><a href="{{ route('comments.create') }}">Crear</a></span></div>
			<div class="menu-option"><span><a href="#">Actualizar</a></span></div>
			<div class="menu-option"><span><a href="#">Borrar</a></span></div>
			<div class="menu-option"><span><a href="#">Lista</a></span></div>
		</div>
	</div>
@stop