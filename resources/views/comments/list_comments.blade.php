@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-xs-7 col-md-8"><h3>Listado de Comentarios</h3></div>
                <div class="col-xs-5 col-md-4">
                  <div class="row">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a href="{{ route('comments.create') }}" class="btn btn-success">Nuevo Comentario</a>
                            </span>
                        </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                  </div><!-- /.row -->
                </div>
            </div>
                <table class="table table-hover">
                      @include('layouts._message')
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nombre</th>
                          <th>E-mail</th>
                          <th>Edad</th>
                          <th>Titulo</th>
                          <th>Comentario</th>
                          <th>Creado</th>
                          <th>Actualizado</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody id="tbody-url" route="{!! route('updateComment') !!}">
                        @foreach ($comments as $comment)
                            
                            <tr>

                              <th scope="row">{{ $comment['id'] }}</th>
                              <td><a href="#" data-pk="{!! $comment['id'] !!}" class="x-name">{!! $comment['name'] !!}</a></td>
                              <td><a href="#" data-pk="{!! $comment['id'] !!}" class="x-mail">{!! $comment['mail'] !!}</a></td>
                              <td><a href="#" data-pk="{!! $comment['id'] !!}" class="x-age">{!! $comment['age'] !!}</a></td>
                              <td><a href="#" data-pk="{!! $comment['id'] !!}" class="x-title">{!! $comment['title'] !!}</a></td>
                              <td><a href="#" data-pk="{!! $comment['id'] !!}" class="x-comment">{!! $comment['comment'] !!}</a></td>
                              <td>{{ $comment['created_at'] }}</td>
                              <td>{{ $comment['updated_at'] }}</td>
                              <td><a href="#myModal" class="btn btn-danger modal-delete" data-toggle="modal" message="¿ Esta seguro que desea borrar el comentario: {{$comment['id']}} ?" route="{!! route('comments.destroy', ['id' => $comment['id'] ]) !!}" >Borrar Comentario</a></td>
                              <!--
                              <td>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['comments.destroy', $comment['id']], 'role' => 'form',]) !!}
                                 <div class="btn-group">
                                  <input type="submit" value="Borrar Comentario" class="btn btn-danger">
                                </div>
                                {!! Form::close() !!}
                              </td> -->
                            </tr>
                        @endforeach
                      </tbody>
                      
                    </table>
                    
              </div>
            </div>
        </div>
    </div>
@endsection