@extends('layouts.app')

@section('content')
	<div id="wrapper-comments">
		<div class="title-comments"><span>Crear Comentarios</span></div>
		
		<div id="wrapper-form">
			<form id="form-create" method="post" action="{{ url('comments') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<label>Nombre:</label><br/>
				<input type="text" id="name" name="name" placeholder="nombre"><br>
				<label>E-mail:</label><br/>
				<input type="email" id="mail" name="mail" placeholder="correo"><br>
				<label>Edad:</label><br/>
				<input type="text" id="age" name="age" placeholder="edad"><br>
				<label>Titulo:</label><br/>
				<input type="text" id="title" name="title" placeholder="title"><br>
				<label>Comentario:</label><br/>
				<textarea name="comment"></textarea><br/>
				<input type="submit" value="Enviar">
			</form>
		</div>
	</div>
@stop